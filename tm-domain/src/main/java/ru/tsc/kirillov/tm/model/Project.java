package ru.tsc.kirillov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class Project extends AbstractWbsModel {

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final User user, @NotNull final String name) {
        super(user, name);
    }

    public Project(@NotNull final User user, @NotNull final String name, @Nullable final String description) {
        super(user, name, description);
    }

    public Project(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        super(name, status, dateBegin);
    }

    public Project(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(user, name, description, dateBegin, dateEnd);
    }

}
