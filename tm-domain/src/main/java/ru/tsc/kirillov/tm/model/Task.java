package ru.tsc.kirillov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public class Task extends AbstractWbsModel {

    @Nullable
    @ManyToOne
    private Project project;

    public Task(@NotNull final User user, @NotNull final String name) {
        super(user, name);
    }

    public Task(@NotNull final User user, @NotNull final String name, @Nullable final String description) {
        super(user, name, description);
    }

    public Task(@NotNull final String name, @NotNull final Status status, @Nullable final Date dateBegin) {
        super(name, status, dateBegin);
    }

    public Task(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super(user, name, description, dateBegin, dateEnd);
    }

}
