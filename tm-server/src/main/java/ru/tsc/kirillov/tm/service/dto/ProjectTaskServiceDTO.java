package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.kirillov.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;

public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    private final IProjectServiceDTO projectService;

    @NotNull
    private final ITaskServiceDTO taskService;

    public ProjectTaskServiceDTO(
            @NotNull final IProjectServiceDTO projectService,
            @NotNull final ITaskServiceDTO taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    private void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId,
            boolean isAdd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(isAdd ? projectId : null);
        taskService.update(task);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        bindTaskToProject(userId, projectId, taskId, true);
    }

    @Override
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        bindTaskToProject(userId, projectId, taskId, false);
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        taskService.removeAllByProjectId(userId, projectId);
        return projectService.removeById(userId, projectId);
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return removeProjectById(userId, project.getId());
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull String[] projects = projectService.findAllId(userId);
        if (projects.length > 0)
            taskService.removeAllByProjectList(userId, projects);
        projectService.clear(userId);
    }

}
