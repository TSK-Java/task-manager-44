package ru.tsc.kirillov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.*;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractWbsModelDTO>
        extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final Class<M> clazz, @NotNull final EntityManager entityManager) {
        super(clazz, entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M create(
            @Nullable final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        @NotNull final M model = clazz.newInstance();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        model.setDateBegin(dateBegin);
        model.setDateEnd(dateEnd);
        return add(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.userId = :userId", getModelName());
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.userId = :userId", getModelName());
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.userId = :userId ORDER BY m.%s",
                getModelName(),
                getColumnSort(comparator)
        );
        return entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return false;
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) = 1 FROM %s m WHERE m.userId = :userId AND m.id = :id",
                getModelName()
        );
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.userId = :userId AND m.id = :id",
                getModelName()
        );
        @NotNull final TypedQuery<M> query = entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("id", id);
        return getResult(query);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final String jpql = String.format("FROM %s m WHERE m.userId = :userId", getModelName());
        @NotNull final TypedQuery<M> query = entityManager.createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setFirstResult(index);
        return getResult(query);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null) return 0;
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) FROM %s m WHERE m.userId = :userId",
                getModelName()
        );
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
