package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.kirillov.tm.api.service.ICommandService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return getServiceLocator().getPropertyService();
    }

    @NotNull
    public ISystemEndpoint geSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
